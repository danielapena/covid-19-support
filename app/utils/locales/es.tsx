export default {
    appTitle: "Soporte durante COVID-19",
    category: "Categoria",
    categorySubtitle: "Seleccione una opcion para continuar",
    subCategorySubtitle: "Por favor seleccione un tipo de producto o servicio que esta siendo vendido a un exorbitante o excesivo precio",
    descriptionSubtitle: "Por favor ingrese una breve descripcion del problema",
    descriptionPlaceholder: "Escriba aqui, 200 caracteres o 29 palabras permitidas.",
};