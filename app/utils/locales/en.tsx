export default {
  appTitle: "COVID-19 Support",
  category: "Category",
  categorySubtitle: "Select an option to continue",
  subCategorySubtitle: "Please select the type of product or service that is being sold at an exorbitant or excessive price.",
  descriptionSubtitle: "Please provide a breaf description of the issue",
  descriptionPlaceholder: "Type here, 200 characters or 29 words allowed.",
};