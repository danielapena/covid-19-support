import { SubcategoryItem } from "./SubcategoryItem";

export interface CategoryItem {
    name: any; // TODO: add interface (language)
    order: string;
    icon?: string | null;
    question: any; // TODO: add interface
    requireDescription?: boolean;
    subcategories?: (SubcategoryItem)[] | null;
}