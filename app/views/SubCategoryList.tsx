import React from 'react';
import { View, Text, TouchableOpacity, StyleSheet, FlatList } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import { SubcategoryItem } from '../models/SubcategoryItem';
import { CategoryItem } from '../models/CategoryItem';
import i18n from '../utils/i18n';

function handleNavigation(category: SubcategoryItem, navigation: any): void { // TODO: Refactor this to handle subcategory recursively
    if(category.subcategories && category.subcategories.length > 0) {
        navigation.navigate('SubCategories', { Category: category});
    } else {
        navigation.navigate('Location', { Category: category})
    }
};

const SubCategoryList = (props: any) => {
    const Category: CategoryItem = props.route.params.Category || null;
    const renderItem = ({ item }) => ( // TODO: Clean up logic and extract component and reuse
        <View style={styles.categoryBox}>
          <TouchableOpacity onPress={() => handleNavigation(item, props.navigation)}>
            <Text>{item.name[i18n.locale]}</Text>
          </TouchableOpacity>
        </View>
      );
    return (
        <SafeAreaView style={styles.container}>
            <View>
                <Text style={styles.title} >{Category.name[i18n.locale]}</Text>
                <Text>{i18n.t("subCategorySubtitle")}</Text>      
            </View>
            <View>
            <FlatList
                    data={Category.subcategories}
                    renderItem={renderItem}
                    keyExtractor={(item, index) => index.toString()}
                />
            </View>
        </SafeAreaView>
    );
};

const styles = StyleSheet.create({
    container: {
      flex: 1,
      marginTop: 0,
      padding: 20,
    },
    categoryBox: {
      flex: 1,
      backgroundColor: '#f9c2ff',
      padding: 20,
      marginVertical: 8,
      marginHorizontal: 0,
    },
    title: {
      fontSize: 32,
    },
  });

export default SubCategoryList; 