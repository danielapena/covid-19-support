import React, { Component } from "react";
import { View, StyleSheet, Dimensions } from 'react-native';
import MapView, {Marker} from 'react-native-maps';
import Geolocation from '@react-native-community/geolocation';

export default class Location extends Component { // TODO: Receive Category and handle Map selection
  constructor(props: any) {
    super(props);
    this.state = {
      latitude: 0,
      longitude: 0,
      error: null
    };
  }

  componentDidMount() {
    Geolocation.getCurrentPosition((position: any) => {
      this.setState({
        latitude: position.coords.latitude,
        longitude: position.coords.longitude,
        error: null
      });
    },
    error => this.setState({ error: error.message}));
  }

  render() {
      return (
        <>
        <View style={styles.container}>
          <MapView style={styles.mapStyle} region={{
            latitude: this.state.latitude,
            longitude: this.state.longitude,
            latitudeDelta: 0.015,
            longitudeDelta: 0.0121
            }} showsUserLocation={true} followsUserLocation={true}>
            <Marker coordinate={this.state} />
          </MapView>
        </View>
        </>
      );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  mapStyle: {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
  },
});