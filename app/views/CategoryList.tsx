import React, {Component} from 'react';
import {View, Text, SafeAreaView, TouchableOpacity, StyleSheet, FlatList, Image } from 'react-native';
import i18n from '../utils/i18n';
import {Categories} from '../assets/categories-fixed.js';
import {CategoryItem} from '../models/CategoryItem';

export default class CategoryList extends Component {
    private _handleNavigation(category: CategoryItem, navigation: any): void { // TODO: Refactor and clean up logic
        if(category.subcategories && category.subcategories.length > 0) {
            navigation.navigate('SubCategories', { Category: category});
        } else {
            category.requireDescription ?  navigation.navigate('Description', {Category: category}) :  navigation.navigate('Location', { Category: category})
        }
    };

    render() {
        const renderCategory = ({item}) => ( // TODO: Clean up logic and reuse component
            <View style={styles.categoryBox}>
              <TouchableOpacity onPress={() => this._handleNavigation(item, this.props.navigation)}>
                <Image style={styles.icon} source={{uri: item.icon }} />
                <Text>{item.name[i18n.locale]}</Text>
              </TouchableOpacity>
            </View>
          );
        return (
        <>
            <SafeAreaView style={styles.container}>
                <View>
                    <Text style={styles.title} >{i18n.t("category")}</Text>
                    <Text>{i18n.t("categorySubtitle")}</Text>
                </View>
                <FlatList
                    data={Categories}
                    renderItem={renderCategory}
                    keyExtractor={item => item.order}
                />                
            </SafeAreaView>            
        </>
        );
    }
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 0,
    padding: 20,
  },
  categoryBox: {
    flex: 1,
    backgroundColor: '#f9c2ff',
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 0,
  },
  icon:{
      width:30,
      height:30,
  },
  title: {
    fontSize: 32,
  },
});