import React, { Component } from "react";
import { SafeAreaView, View, Text, StyleSheet, TextInput, Button } from 'react-native';
import i18n from "../utils/i18n";
import { CategoryItem } from "../models/CategoryItem";

export default class Description extends Component {
    render() {
      // TODO: Refactor and clean up logic to handle incoming from SubCategory or Category, Implement next button, Capture Description input
      const item: CategoryItem = this.props.route.params.Category || null;
        return (
        <>
          <SafeAreaView style={styles.container}>
            <View>
                <Text style={styles.title} >{item.name[i18n.locale]}</Text>
                <Text>{i18n.t("descriptionSubtitle")}</Text>
                <TextInput
                  maxLength={200}
                  style={{ height: 40, borderColor: 'gray', borderWidth: 1 }}
                  placeholder={i18n.t('descriptionPlaceholder')}
                />
                <Button title="Next" />
            </View>
          </SafeAreaView>
        </>
        );
    }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 0,
    padding: 20,
  },
  title: { // TODO: extract to common CSS
    fontSize: 32,
  }
});