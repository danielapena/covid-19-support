# Mobile Skill Test

The main objective is to create an application that simulates the consumption of Firestore
information. We expect a list of categories, and support subcategories retrieval, description view
and map view for “Report Price Gouging”.

Category has some optional values like “requireDescription” that enable some screens on the
app.

Also we expect a map view where we can get a report address using user location or allowing
user search for an address. Please see Expected on App.
For more reference see: https://covid19.heyirys.com/app

## Documents Structure on Firebase

### Categories

- name: 
A map with english (“en”) and spanish (“es”) values to display on user interface.
- order: 
Numeric value that represents expected order in user interface.

- question:
Map with english and spanish values to display as the title of the list of subcategories.

strictly confidential. v1.0

- requireDescription:
If this value is true means current category require a description

### Subcategories
A collection of possible selections for selected category. A subcategory can have
another subcategory collection, behaves like a tree until the selection has no
subcategories, in this case the child or end node can have a collection of custom
questions we call e-forms.
- name:
Map with english and spanish values to display.
- icon:
String base64 of subcategory icon.
- url:
Optional url of site if category type is “redirect”

## Expected design

![Expected Design]( img/readme-expected-design.jpg)

## Expected on app
- Category list. 1
- Subcategory list. 1.1
- Description view. 1.2
- Map view. 2
- Extra: How can we measure/track user activity in the app ?

## General considerations
- Project structure.
- Use any architectural pattern.
- Use libraries you think are convenient.
- Translation service (i18n) for english and spanish. Also use values from Firebase (“en”
and “es”)
- Use the json file attached to the email to read the category information, this dataset is
similar to what you could find in Firebase.
- Upload and share the Bitbucket repository.

## Technical specifications

- React Native 0.60+.
- App should run on IOS 10.0 and above.
- App should run on Android API 23 and above.