import React from 'react';
import CategoryList from './app/views/CategoryList';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Location from './app/views/Location';
import Description from './app/views/Description';
import SubCategoryList from './app/views/SubCategoryList';
import i18n from './app/utils/i18n';

const Stack = createStackNavigator();

export default class App extends React.Component {
  render() {
    return (
      <NavigationContainer>
        <Stack.Navigator>
          <Stack.Screen name="Categories" component={CategoryList} options={{ title: i18n.t("appTitle")}} />
          <Stack.Screen name="Location" component={Location} options={{ title: i18n.t("appTitle")}} />
          <Stack.Screen name="SubCategories" component={SubCategoryList} options={{ title: i18n.t("appTitle")}} />
          <Stack.Screen name="Description" component={Description} options={{ title: i18n.t("appTitle")}} />
        </Stack.Navigator>
      </NavigationContainer>
    );
  }
}